import uiManager
import sys
from PySide6.QtWidgets import QApplication


if __name__ == '__main__':
    app = QApplication([])
    ui = uiManager.UiManager()
    app.installEventFilter(ui)
    ret = app.exec_()
    app.removeEventFilter(ui)
    sys.exit(ret)
